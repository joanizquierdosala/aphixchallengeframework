
## Aphix Challenge

Build an MVC application using PHP and a popular framework  which will use the YouTube API on the server side to return a list of YouTube search results by an ajax call.

Some coding restrictions:
The code should be created using any popular PHP framework such as Zend Framework 3, Laravel or Symfony and follow their best practices for code styling.

The code submission must be done by sending through a git repository with at least two commits:
1. Showing the basic framework and library installation with no modifications
2. Showing the application being built and committed without any basic framework setup, this may be done on several commits if desired showing clear progression in building the app.

The frontend isn't a focus point of this, however it should be presentable and usable in whichever way you seem fit.

## Framework

Laravel v6.8.0

## Code Editor

Visual Studio Code

## Libraries

- Laravel PHP Facade/Wrapper for the Youtube Data API

	https://github.com/alaouy/Youtube
	https://github.com/alaouy/Youtube/issues/5 (be carefull with SSL certificate)

- A PHP client library for accessing Google APIs

	https://github.com/googleapis/google-api-php-client

- jQuery 

	https://github.com/jquery/jquery
	https://github.com/jquery-validation/jquery-validation

## Repository

	BitBucket (GIT repository)
	https://joanizquierdosala@bitbucket.org/joanizquierdosala/aphixchallengeframework.git

## Download Repository

	git clone https://joanizquierdosala@bitbucket.org/joanizquierdosala/aphixchallengeframework.git AphixChallengeFramework
	cd AphixChallengeFramework
	composer install
	copy .env.example .env
	php artisan key:generate

	php artisan serve
	http://localhost:8000/youtube-form

