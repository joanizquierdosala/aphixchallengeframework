<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use Alaouy\Youtube\Facades\Youtube;
use Google_Client;
use Google_Service_Youtube;

class YoutubeController extends Controller
{

    public function index()
    {
        return \View::make('youtube-index');
    }

    public function list(Request $request)
    {

        $q = $request->input('keyword');

        /*
        $params = [
            'q'             => $q,
            'type'          => 'video',
            'part'          => 'id, snippet',
            'maxResults'    => 20
        ];
        //Youtube::setApiKey('');
        $search = Youtube::searchAdvanced($params, true);
        foreach ($search['results'] as $item) {
            //print_r($item);
            //echo $item->id->videoId.'<br>';
            //echo $item->snippet->title.'<br>';
            //echo $item->snippet->description.'<br>';
            //echo $item->snippet->thumbnails->medium->url.'<br>';
        }
        $view = view("youtube-list",['items' => $search['results']])->render();
        return response()->json( array('success' => true, 'html'=>$view) );
        */

        $client = new Google_Client();
        $client->setApplicationName("AphixChallenge");
        $client->setDeveloperKey("AIzaSyAiOYxho5wlcbrxN9tGgpZ5sk7AtGkF70s");
        $service = new Google_Service_YouTube($client);
        $queryParams = [
            'q'             => $q,
            'maxResults'    => 20
        ];
        $response = $service->search->listSearch('id, snippet', $queryParams);
        //print_r($response['items']);
        $view = view("youtube-list",['items' => $response['items']])->render();
        return response()->json( array('success' => true, 'html'=>$view) );

    }

}
