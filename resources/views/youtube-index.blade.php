<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel</title>
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <script src="//ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.js"></script>  
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"></script>  
        <style>
            html, body {
                background-color: #3A97FC;
                color: #fff;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
                font-size: 15px; 
                text-align:center;
            }
            .content { 
                margin:0 auto;
                max-width: 860px;
            }
            input {
                padding: 14px 50px;
                color: #1b3e6f;
                background-color: #ffffff;
                border-radius: 6px;
                border: none;
            } 
            button { 
                padding: 14px 50px;
                color: #fff;
                background-color: #1b3e6f;
                border-radius: 6px;
                border: none;
                cursor: pointer;
            }
            ul.youtube-list {
                list-style-type: none; 
                display: flex;
                flex-direction: row;
                flex-wrap: wrap;
                justify-content: center;
                align-content: flex-start;
                align-items: flex-start;
            }
            li {
                margin: 10px;
                padding: 20px;
                color: #1b3e6f;
                background-color: #fff;
                width: 200px;
                border-radius: 6px;
                word-wrap: break-word;
                cursor: pointer;
            }
            li:hover {
                background-color: #f0f0f0;
            }
            img {
                width:200px;
                height:auto;
            }
        </style>
    </head>
<body>
<div class="content">  
    <h1>Challenge to Aphix Software</h1>
    <p>Build an MVC application using PHP and a popular framework  which will use the YouTube API on the server side to return a list of YouTube search results by an ajax call.</p>
    <form id="youtube_form" method="post" action="javascript:void(0)">
        @csrf
        <p><input type="text" name="keyword" placeholder="Please enter a keyword"></p>
        <p><button type="submit" id="send_form">SUBMIT</button></p>
    </form>
    <div id="youtube_view">...</div>
</div>
<script>
    if ($("#youtube_form").length > 0) {
        $("#youtube_form").validate({
            rules: {
                keyword: {
                    required: true,
                    maxlength: 50
                }  
            },
            messages: {
                keyword: {
                    required: "Please enter a keyword",
                    maxlength: "Your keyword maxlength should be 50 characters long."
                }
            },
            submitHandler: function(form) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url:"{{ route('youtube-search.post') }}",
                    type: "POST",
                    data: $('#youtube_form').serialize(),
                    success: function( response ) {
                        //alert(response);
                        $('#youtube_view').html(response.html);
                    }
                });
            }
        })
    }
</script>
</body>
</html>