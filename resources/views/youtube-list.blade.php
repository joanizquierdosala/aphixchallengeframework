<ul class="youtube-list">
@foreach ($items as $item)
    <li onclick="window.open('https://www.youtube.com/watch?v={{ $item->id->videoId }}','_blank')">
        <img src="{{ $item->snippet->thumbnails->medium->url }}">
        <h2>{{ $item->snippet->title }}</h2>
        <p>{{ $item->snippet->description }}<p>
    </li>
@endforeach
</ul>
